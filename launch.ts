import express from "express";
import path from "path";
import http from "http";
import socketIO from "socket.io";

import { startSocketComunication } from "./server/chat/chat-config";

const app = express();

const server = http.createServer(app);
const io = socketIO(server);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", (req: any, res: any) => {
  res.json({ msg: "This is my chatt app, and that's good" });
});

const port = process.env.PORT || 4000;

startSocketComunication(io);

app.listen(port, () => {
  console.log(`Chat App Broadcasting at port: ${port}`);
});

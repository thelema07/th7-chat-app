import React, { useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { ChatModal } from "./chat-util/ChatModal";

export const Chat = () => {
  useEffect(() => {
    return () => {
      localStorage.clear();
    };
  });

  return (
    <Container>
      {<ChatModal />}
      <Row className="pt-5">
        <Col lg={9}>
          <div className="d-flex justify-content-between">
            <h1>Chat</h1>
            <h1>{localStorage.getItem("chat-user")}</h1>
          </div>
          <div>
            <Form>
              <Form.Group controlId="text-area1">
                <Form.Control as="textarea" rows="3" />
              </Form.Group>
              <Button size="sm" variant="primary">
                Comment
              </Button>
            </Form>
          </div>

          <div className="pt-3 mt-3">
            <Card body>
              <h1>Hello</h1>
            </Card>
          </div>
        </Col>
        <Col lg={3}>
          <h4>Connected Users</h4>
        </Col>
      </Row>
    </Container>
  );
};

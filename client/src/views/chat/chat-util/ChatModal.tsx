import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

export const ChatModal = () => {
  const [showModal, setShow] = useState(false);
  const [chatUser, setChatUser] = useState("");
  const [chatErrorMessage, setChatErrorMessage] = useState("");

  const validateChatUser = () => {
    if (chatUser === "") {
      setChatErrorMessage("Invalid User");
    } else {
      setChatUser(chatUser);
      localStorage.setItem("chat-user", chatUser);
      setShow(false);
    }
  };

  useEffect(() => {
    let getchatUser = localStorage.getItem("chat-user");
    if (getchatUser === "" || !getchatUser) {
      setShow(true);
    }
  }, [chatUser]);

  return (
    <>
      <Modal show={showModal} onHide={() => setShow(false)} backdrop="static">
        <Modal.Header className="ml-4 mr-4 mt-3" closeButton={false}>
          <Modal.Title className="text-primary">
            <strong>Opss!! You're new... (Tootips!!!)</strong>
          </Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form className="ml-4 mr-4">
            <Form.Group controlId="formChat">
              <Form.Label>
                <strong className="text-primary">Chat User</strong>
              </Form.Label>
              <Form.Control
                value={chatUser}
                onChange={(e: any) => setChatUser(e.target.value)}
                type="text"
                placeholder="User"
              />
              <Form.Control.Feedback type="invalid">
                Please choose a username.
              </Form.Control.Feedback>
              <Form.Text>
                <p className="text-danger">{chatErrorMessage}</p>
              </Form.Text>
            </Form.Group>
          </Form>
        </Modal.Body>

        <Modal.Footer className="ml-4 mr-4 mb-3">
          <Button variant="secondary" onClick={() => setShow(false)}>
            <Link to="/">Go Back</Link>
          </Button>
          <Button variant="primary" onClick={validateChatUser}>
            Enter
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

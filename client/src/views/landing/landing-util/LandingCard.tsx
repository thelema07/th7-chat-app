import React, { FC } from 'react';
import Card from 'react-bootstrap/Card';

interface LandingCardProps {
  cardText: string;
  icon: any;
}

export const LandingCard: FC<LandingCardProps> = ({ cardText, icon }) => {
  return (
    <Card bg="primary" text="white" className="shadow mb-5">
      <Card.Body className="text-right d-flex justify-content-between">
        <Card.Title className="d-flex justify-content-start">
          <h1 className="display-2 m-0">{icon}</h1>
        </Card.Title>
        <Card.Text>{cardText}</Card.Text>
      </Card.Body>
    </Card>
  );
};

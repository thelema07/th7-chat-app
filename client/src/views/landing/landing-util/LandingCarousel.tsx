import React, { Fragment } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image';
import Col from 'react-bootstrap/Col';

import styles from '../Landing.module.scss';

import Mobx from '../../../img/mobx.png';
import Mongo from '../../../img/mongodb.png';
import ReactJS from '../../../img/react-js.png';
import Sass from '../../../img/sass.png';

export const LandingCarousel: React.FC = () => {
  const images = [Mongo, ReactJS, Sass, Mobx];
  return (
    <Fragment>
      <Col className="pb-3">
        <Carousel
          className={styles.carouselContainer}
          controls={false}
          indicators={false}
          interval={3000}
          keyboard={true}
          pauseOnHover={true}
          slide={true}
        >
          {images.map(img => (
            <Carousel.Item>
              <Image src={img} />
            </Carousel.Item>
          ))}
        </Carousel>
      </Col>
    </Fragment>
  );
};

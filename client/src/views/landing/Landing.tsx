import React from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Badge from 'react-bootstrap/Badge';
import Alert from 'react-bootstrap/Alert';

import { DiJsBadge, DiSass, DiNodejs } from 'react-icons/di';

import { LandingCard } from './landing-util/LandingCard';
import { LandingCarousel } from './landing-util/LandingCarousel';

export const Landing = () => {
  const cardProps = [
    {
      title: 'Typescript',
      subtitle: 'The Language of the Web',
      text:
        "Some quick example text to build on the card title and make up the bulk of the card's content.",
      icon: <DiJsBadge />
    },
    {
      title: 'NodeJs',
      subtitle: 'Backend using Typescript',
      text:
        "Some quick example text to build on the card title and make up the bulk of the card's content.",
      icon: <DiNodejs />
    },
    {
      title: 'SASS',
      subtitle: 'SCSS Features',
      text:
        "Some quick example text to build on the card title and make up the bulk of the card's content.",
      icon: <DiSass />
    }
  ];

  return (
    <Container>
      <Row>
        <Col>
          <Alert variant="primary" className="mt-5 text-center">
            A single chat app using <Badge variant="primary">ReactJS</Badge> &{' '}
            <Badge variant="primary">NodeJS</Badge>
          </Alert>
        </Col>
      </Row>
      <Row className="d-flex justify-content-center align-items-center">
        <Col>
          <h1 className="display-1 text-center">
            Chat <Badge variant="primary">TS</Badge>
          </h1>
        </Col>
        <LandingCarousel />
      </Row>

      <Row>
        {cardProps.map(card => (
          <Col>
            <LandingCard cardText={card.text} icon={card.icon} />
          </Col>
        ))}
      </Row>
    </Container>
  );
};

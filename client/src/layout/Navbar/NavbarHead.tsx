import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { MdChat } from 'react-icons/md';
import { Link } from 'react-router-dom';
import { FaHome, FaCogs } from 'react-icons/fa';
import Nav from 'react-bootstrap/Nav';

export const NavbarHead = () => {
  return (
    <>
      <Navbar
        sticky="top"
        bg="primary"
        className="d-flex justify-content-center shadow"
      >
        <Nav>
          <Nav.Item>
            <Link to="/">
              <Nav.Link className="text-white" href="/home">
                <FaHome size="1.5rem" />
              </Nav.Link>
            </Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/chat">
              <Nav.Link className="text-white" href="/home">
                <MdChat size="1.5rem" />
              </Nav.Link>
            </Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/chat">
              <Nav.Link className="text-white" href="/home">
                <FaCogs size="1.5rem" />
              </Nav.Link>
            </Link>
          </Nav.Item>
        </Nav>
      </Navbar>
    </>
  );
};

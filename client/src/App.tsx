import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { NavbarHead } from "./layout/Navbar/NavbarHead";
import { Landing } from "./views/landing/Landing";
import { Chat } from "./views/chat/Chat";

const App: React.FC = () => {
  return (
    <div>
      <Router>
        <Fragment>
          <NavbarHead />
          <Route exact path="/" component={Landing} />
          <section>
            <Switch>
              <Route exact path="/chat" component={Chat} />
            </Switch>
          </section>
        </Fragment>
      </Router>
    </div>
  );
};

export default App;
